<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
    <div class="row">
    </div>
    <div class="card">
        <div class="col-lg-6">

            <?= form_error(
                'menu',
                '<div class="alert alert-danger" role="alert">',
                '</div>'
            ); ?>

            <?= $this->session->flashdata('message'); ?>
        </div>
        <div class="card-body">

            <form action="<?= base_url('c_kelola_pengiriman') ?>" method="post">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="kode_transaksi">Kode Transaksi</label>
                            <input type="text" class="form-control" id="kode_transaksi" name="kode_transaksi" placeholder="Scan Kode Transaksi">
                            <small><i>Ketika scan akan langsung otomatis input data</i></small>
                        </div>
                    </div>
                </div>
            </form>
            <table class="table table-striped" id="dataTable">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Kode Transaksi</th>
                        <th>Model</th>
                        <th>Line</th>
                        <th>Style</th>
                        <th>Gender</th>
                        <th>Size</th>
                        <th>Jumlah</th>
                        <th>keterangan</th>

                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no = 1;
                    foreach ($stock as $s) : ?>
                        <tr>
                            <td><?= $no++; ?></td>
                            <td>
                                <?php
                                require 'vendor/autoload.php';
                                $generator = new Picqer\Barcode\BarcodeGeneratorPNG();
                                echo '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($s['kode_transaksi'], $generator::TYPE_CODE_128)) . '">';
                                ?></td>
                            <td><?= $s['nama_barang'] ?></td>
                            <td><?= $s['line'] ?></td>
                            <td><?= $s['nama_style'] ?></td>
                            <td><?= $s['gender'] ?></td>
                            <td><?= $s['size'] ?></td>
                            <td><?= $s['stok_keluar'] ?></td>



                            <?php if ($s['status'] == 0) : ?>
                                <td>
                                    <p class="badge badge-primary">Belum dikirim</p>
                                </td>
                                
                            <?php else : ?>
                                <td>
                                    <p class="badge badge-primary">Sudah dikirim</p>
                                </td>
                            <?php endif; ?>
                        </tr>

                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->