<!DOCTYPE html>
<html lang="en"><head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title>Print Label</title>
    <style>
        table {
            width: 100%;
        }
        th, td {
            padding: 10px;
        }
        td {
            text-align: center;
        }
        img{
            width: 100%;
        }
    </style>
</head><body>

    <table border="1">
        <tr>
            <td colspan="3"><?= $transaksi['line']; ?></td>
            <td><?= $transaksi['nama_barang']; ?></td>
        </tr>
        <tr>
            <td>Gen</td>
            <td>Size</td>
            <td>Qty</td>
            <td rowspan="2"><?= $transaksi['nama_style']; ?></td>
        </tr>
        <tr>
            <td><?= $transaksi['gender']; ?></td>
            <td><?= $transaksi['size']; ?></td>
            <td><?= $transaksi['stok_keluar']; ?></td>
        </tr>
        <tr>
            <td colspan="4">

                <?php
    
                use Carbon\Carbon;
    
                require 'vendor/autoload.php';
                $generator = new Picqer\Barcode\BarcodeGeneratorPNG();
                echo '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($transaksi['kode_transaksi'], $generator::TYPE_CODE_128)) . '">'
    
                ?><br>
                <?= $transaksi['kode_transaksi']; ?>
            </td>
        </tr>
        <!-- <tr>
            <td rowspan="4></td>
            <td>Nama Admin : <?= $transaksi['nama']; ?></td>

        </tr>
        <tr>
            <td>Nama Barang : <?= $transaksi['nama_barang']; ?></td>
        </tr>
        <tr>

            <td>Jumlah Barang : <?= $transaksi['stok_keluar']; ?></td>
        </tr>
        <tr>

            <td>Tanggal Ajuan : <?=
                                Carbon::parse($transaksi['tanggal'])->translatedFormat('d F Y'); ?></td>
        </tr> -->
    </table>
</body></html>