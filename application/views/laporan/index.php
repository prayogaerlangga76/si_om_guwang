<div class="container-fluid">
</div>

<div class="card">
    <div class="col-lg-6">

        <?= form_error(
            'menu',
            '<div class="alert alert-danger" role="alert">',
            '</div>'
        ); ?>

        <?= $this->session->flashdata('message'); ?>
    </div>
    <div class="card-body">
        <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

        <table class="table table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Barang</th>
                    <th>Tanggal Scan</th>
                    <th>Tanggal Exp</th>
                    <th>Stock</th>

                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                foreach ($laporan as $l) : ?>
                    <tr>
                        <td><?= $no++; ?></td>
                        <td><?= $l['nama_barang']; ?></td>
                        <td><?= $l['tgl_scan']; ?></td>
                        <td><?= $l['tgl_exp']; ?></td>
                        <td><?= $l['sisa_stok']; ?></td>
                    </tr>

                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<hr>

<div class="card">
    <div class="col-lg-6">

        <?= form_error(
            'menu',
            '<div class="alert alert-danger" role="alert">',
            '</div>'
        ); ?>

        <?= $this->session->flashdata('message'); ?>
    </div>
    <div class="card-body">
    <h1 class="h3 mb-4 text-gray-800">Tabel Permintaan</h1>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Kode Transaksi</th>
                    <th>Model</th>
                    <th>Style</th>
                    <th>Gender</th>
                    <th>Size</th>
                    <th>Jumlah</th>

                </tr>
            </thead>
            <tbody>
                <?php
                use Carbon\Carbon;
                $no = 1;
                foreach ($permintaan as $s) : ?>
                    <tr>
                        <td><?= $no++; ?></td>
                        <td>
                            <?php
                            require 'vendor/autoload.php';
                            $generator = new Picqer\Barcode\BarcodeGeneratorPNG();
                            echo '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($s['kode_transaksi'], $generator::TYPE_CODE_128)) . '">';
                            ?></td>
                        <td><?= $s['nama_barang'] ?></td>
                        <td><?= $s['nama_style'] ?></td>
                        <td><?= $s['gender'] ?></td>
                        <td><?= $s['size'] ?></td>
                        <td><?= $s['stok_keluar'] ?></td>
                    </tr>

                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<hr>
<div class="card">
    <div class="col-lg-6">

        <?= form_error(
            'menu',
            '<div class="alert alert-danger" role="alert">',
            '</div>'
        ); ?>

        <?= $this->session->flashdata('message'); ?>
    </div>
    <div class="card-body">
    <h1 class="h3 mb-4 text-gray-800">Tabel Persediaan</h1>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Kode Transaksi</th>
                    <th>Model</th>
                    <th>Style</th>
                    <th>Gender</th>
                    <th>Size</th>
                    <th>Jumlah</th>
                    <th>Tanggal Scan</th>
                    <th>Tanggal Exp</th>
                </tr>
            </thead>
            <tbody>
                <?php
                require 'vendor/autoload.php';
                $no = 1;
                foreach ($persediaan as $s) : ?>
                    <tr>
                        <td><?= $no++; ?></td>
                        <td><?php 
                        require 'vendor/autoload.php';
                        $generator = new Picqer\Barcode\BarcodeGeneratorPNG();
                        echo '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($s['kode_transaksi'], $generator::TYPE_CODE_128)) . '">';?>
                        <td><?= $s['nama_barang'] ?></td>
                        <td><?= $s['nama_style'] ?></td>
                        <td><?= $s['gender'] ?></td>
                        <td><?= $s['size'] ?></td>
                        <td><?= $s['stok_masuk'] ?> Pasang</td>
                        <td><?= Carbon::parse($s['tgl_scan'])->translatedFormat('d F Y'); ?></td>
                        <td><?= Carbon::parse($s['tgl_exp'])->translatedFormat('d F Y') ?></td>
                    </tr>

                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<hr>
<div class="card">
    <div class="col-lg-6">

        <?= form_error(
            'menu',
            '<div class="alert alert-danger" role="alert">',
            '</div>'
        ); ?>

        <?= $this->session->flashdata('message'); ?>
    </div>
    <div class="card-body">
        <h2 class="h3 mb-4 text-gray-800">Tabel Pengguna</h2>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Pengguna</th>
                    <th>Role</th>

                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                foreach ($pengguna as $um) : ?>
                    <tr>
                        <td><?= $no++; ?></td>
                        <td><?= $um['username'] ?></td>
                        <td><?= $um['role'] ?></td>

                    </tr>

                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
</div>