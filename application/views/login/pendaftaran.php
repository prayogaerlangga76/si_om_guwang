<div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5 col-lg-7 mx-auto">
        <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
                <div class="col-lg">
                    <div class="p-5">
                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4">Pendaftaran akun Sistem Pembayaran Rekening Air KP-Spam</h1>
                        </div>
                        <form class="user" method="post" action="<?= base_url('auth/pendaftaran'); ?>">
                            <div class="form-group">
                                <label for="rek_pelanggan">No. Rekening</label>
                                <input type="text" class="form-control " id="rek_pelanggan" value="<?= $pelangganMax ?>" name="rek_pelanggan" placeholder="No. Rekening Pelanggan" readonly>
                            </div>
                            <div class="form-group">
                                <label for="nama_pelanggan">Nama Lengkap</label>
                                <input type="text" class="form-control " id="nama_pelanggan" name="nama_pelanggan" placeholder="Nama Lengkap" value="<?= set_value('nama_pelanggan') ?>">
                                <?= form_error('nama_pelanggan', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                            <div class="form-group">
                            <label for="alamat">Alamat</label>
                                <textarea name="alamat" id="alamat" class="form-control" rows="5" placeholder="Alamat" value=""><?= set_value('alamat') ?></textarea>
                                <?= form_error('alamat', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                            <div class="form-group">
                            <label for="no_telepon">No. Telepon</label>
                                <input type="number" class="form-control " id="no_telepon" name="no_telepon" placeholder="No. Telepon" value="<?= set_value('no_telepon') ?>">
                                <?= form_error('no_telepon', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                            <div class="form-group">
                            <label for="email">Email</label>
                                <input type="email" class="form-control " id="email" name="email" placeholder="Email" value="<?= set_value('email') ?>">
                                <?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                            <div class="form-group">
                                <label for="tarif_id">Jenis Layanan</label>
                                <select name="tarif_id" id="tarif_id" class="form-control">
                                    <option value="" disabled selected>Pilih Layanan</option>
                                    <?php foreach ($tarif as $t) : ?>
                                        <option value="<?= $t['id'] ?>"><?= $t['kategori'] . " || Rp. " . $t['tarif']; ?> /M<sup>3</sup></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                            <label for="username">Username</label>
                                <input type="text" class="form-control " id="username" name="username" placeholder="Username" value="<?= set_value('username') ?>">
                                <?= form_error('username', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                <label for="password1">Password</label>
                                <input type="password" class="form-control " id="password1" name="password1" placeholder="Password">
                                <?= form_error('password1', '<small class="text-danger pl-3">', '</small>'); ?>
                            </div>
                            <div class="col-sm-6">
                                    <label for="password2">Konfirmasi Password</label>
                                    <input type="password" class="form-control " id="password2" name="password2" placeholder="Konfirmasi Password">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary btn-user btn-block">
                                Daftar Akun
                            </button>
                        </form>
                        <hr>
                        <div class="text-center">
                            <a class="small" href="forgot-password.html">Forgot Password?</a>
                        </div>
                        <div class="text-center">
                            <a class="small" href="<?= base_url('auth') ?>">Saya sudah punya akun!</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>