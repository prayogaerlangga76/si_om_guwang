<div class="container-fluid">
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
</div>

<div class="card">
    <div class="col-lg-6">

        <?= form_error(
            'menu',
            '<div class="alert alert-danger" role="alert">',
            '</div>'
        ); ?>

        <?= $this->session->flashdata('message'); ?>
    </div>
    <div class="card-body">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Barang</th>
                    <th>Tanggal Scan</th>
                    <th>Tanggal Exp</th>
                    <th>Total Stok</th>
                    
                </tr>
            </thead>
            <tbody>
            <?php
               
                $no = 1;
                foreach ($laporan as $l) : ?>
                    <tr>
                        <td><?= $no++; ?></td>
                        <td><?= $l['nama_barang']?></td>
                        <td></td>
                        <td></td>
                        <td><?= ($l['stok_masuk'] - $l['stok_keluar']) ?></td>
                        
                    </tr>

                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

</div>


