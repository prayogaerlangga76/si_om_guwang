<div class="container-fluid">
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
</div>

<div class="card">
    <div class="col-lg-6">

        <?= form_error(
            'menu',
            '<div class="alert alert-danger" role="alert">',
            '</div>'
        ); ?>

        <?= $this->session->flashdata('message'); ?>
    </div>
    <div class="card-body">
        <a href="" class="btn btn-primary mb-3" data-toggle="modal" data-target="#newMenuModal"> Tambah Stok Barang </a>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Kode Transaksi</th>
                    <th>Model</th>
                    <th>Style</th>
                    <th>Gender</th>
                    <th>Size</th>
                    <th>Jumlah</th>
                    <th>Tanggal Scan</th>
                    <th>Tanggal Exp</th>
                    
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php
                use Carbon\Carbon;
                require 'vendor/autoload.php';
                $no = 1;
                foreach ($stock as $s) : ?>
                    <tr>
                        <td><?= $no++; ?></td>
                        <td><?php 
                        require 'vendor/autoload.php';
                        $generator = new Picqer\Barcode\BarcodeGeneratorPNG();
                        echo '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($s['kode_transaksi'], $generator::TYPE_CODE_128)) . '">';?>
                        <td><?= $s['nama_barang'] ?></td>
                        <td><?= $s['nama_style'] ?></td>
                        <td><?= $s['gender'] ?></td>
                        <td><?= $s['size'] ?></td>
                        <td><?= $s['stok_masuk'] ?></td>
                        <td><?= Carbon::parse($s['tgl_scan'])->translatedFormat('d F Y'); ?></td>
                        <td><?= Carbon::parse($s['tgl_exp'])->translatedFormat('d F Y') ?></td>
                        <td>
                            <a href="#" class="badge badge-warning" data-toggle="modal" data-target="#editMenuModal<?= $s['kode_transaksi']; ?>">Edit</a>
                        </td>
                    </tr>

                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

</div>

<div class="modal fade" id="newMenuModal" tabindex="-1" role="dialog" aria-labelledby="newMenuModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newMenuModalLabel">Tambah Stok Barang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('masterstock') ?>" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="kode_transaksi">Kode Transaksi</label>
                        <input type="text" class="form-control" id="kode_transaksi" name="kode_transaksi" value="<?= $transaksi_max; ?>" placeholder="Kode Transaksi" readonly>
                    </div>
                    <div class="form-group">
                        <label for="kode_barang">Kode Barang</label>
                        <input type="text" class="form-control" id="kode_barang" name="kode_barang" placeholder="Kode Barang">
                    </div>
                    <div class="form-group">
                        <input type="hidden" class="form-control" id="barang_id" name="barang_id"  placeholder="Nama Barang">
                        <label for="nama_barang">Nama Barang</label>
                        <input type="text" class="form-control" id="nama_barang" name="nama_barang"  placeholder="Nama Barang" readonly>
                    </div>
                    <div class="form-group">
                        <label for="stock">Stok Saat Ini</label>
                        <input type="number" class="form-control" id="stock" name="stock" placeholder="Stock Barang" readonly>
                    </div>
                    <div class="form-group">
                        <label for="style_id">Style</label>
                        <select name="style_id" id="style_id" class="form-control">
                            <option value="" selected disabled>Pilih Style</option>
                            <?php foreach ($style as $s) :?>
                            <option value="<?= $s['id']; ?>"><?= $s['nama_style']; ?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="size">Size</label>
                        <select name="size" id="size" class="form-control">
                            <option value="" selected disabled>Pilih Size</option>
                            <?php 
                            for ($i=3; $i <= 15; $i++) {
                            ?>
                            <option value="<?= $i; ?>"><?= $i; ?></option>
                            <?php }?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="gender_id">Gender</label>
                        <select name="gender_id" id="gender_id" class="form-control">
                            <option value="" selected disabled>Pilih Style</option>
                            <?php foreach ($gender as $g) :?>
                            <option value="<?= $g['id']; ?>"><?= $g['gender']; ?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="stock_masuk">Stok Masuk</label>
                        <input type="number" class="form-control" id="stock_masuk" name="stock_masuk" placeholder="Stok Masuk" required>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php $i = 0;
foreach ($stock as $s) : $i++; ?>
    <div class="modal fade" id="editMenuModal<?= $s['kode_transaksi']; ?>" tabindex="-1" role="dialog" aria-labelledby="editMenuModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editMenuModalLabel">Edit Barang Masuk</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?= base_url('masterstock/update/' . $s['kode_transaksi']) ?>" method="post">
                    <input type="hidden" name="id" value="<?= $s['kode_transaksi']; ?>">
                    <div class="modal-body">
                    <div class="form-group">
                        <label for="kode_transaksi">Kode Transaksi</label>
                        <input type="text" class="form-control" id="kode_transaksi" name="kode_transaksi" value="<?= $s['kode_transaksi']?>" placeholder="Kode Transaksi" readonly>
                    </div>
                    <div class="form-group">
                        <label for="barang_id">Nama Barang</label>
                        <select name="barang_id" id="barang_id" class="form-control">
                            <option value="" selected disabled>Pilih Barang</option>
                            <?php foreach ($barang as $b) : ?>
                                <?php if ($b['id'] == $s['barang_id']) : ?>
                                <option value="<?= $b['id']; ?>" selected><?= $b['nama_barang']; ?></option>
                                <?php else :?>
                                    <option value="<?= $b['id']; ?>"><?= $b['nama_barang']; ?></option>
                                <?php endif;?>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="stock_masuk">Stok Barang</label>
                        <input type="number" class="form-control" id="stock_masuk" name="stock_masuk" value="<?= $s['stok_masuk']?>" placeholder="Stock Barang" required>
                    </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </form>
            </div>
        </div>

<?php endforeach; ?>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type='text/javascript'>
    $(document).on('change', '#kode_barang', function() {
        $.ajax({
            type: "GET",
            url: '<?= base_url('/masterstock/getstock/'); ?>' + $('#kode_barang').val(),
            dataType: "json",
            success: function(response) {
                $('#nama_barang').html("");
                $('#stock').html("");
                $('#barang_id').html("");
                $('#nama_barang').val(response.nama_barang);
                $('#stock').val(response.stock);
                $('#barang_id').val(response.id);
            }

        });
    });
</script>