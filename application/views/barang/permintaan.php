<div class="container-fluid">
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
</div>

<div class="card">
    <div class="col-lg-6">

        <?= form_error(
            'menu',
            '<div class="alert alert-danger" role="alert">',
            '</div>'
        ); ?>

        <?= $this->session->flashdata('message'); ?>
    </div>
    <div class="card-body">
        <?php if ($user['role_id'] == 4 || $user['role_id'] == 1) : ?>
            <a href="" class="btn btn-primary mb-3" data-toggle="modal" data-target="#newMenuModal"> Tambah Permintaan Barang</a>
        <?php endif; ?>
        <table class="table table-striped" id="dataTable">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Kode Transaksi</th>
                    <th>Model</th>
                    <th>Style</th>
                    <th>Gender</th>
                    <th>Size</th>
                    <th>Jumlah</th>


                    <?php if ($user['role_id'] == 2 || $user['role_id'] == 4 || $user['role_id'] == 1) : ?>
                        <th>Aksi</th>
                    <?php endif; ?>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                foreach ($stock as $s) : ?>
                    <tr>
                        <td><?= $no++; ?></td>
                        <td>
                            <?php
                            require 'vendor/autoload.php';
                            $generator = new Picqer\Barcode\BarcodeGeneratorPNG();
                            echo '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($s['kode_transaksi'], $generator::TYPE_CODE_128)) . '">';
                            ?></td>
                        <td><?= $s['nama_barang'] ?></td>
                        <td><?= $s['nama_style'] ?></td>
                        <td><?= $s['gender'] ?></td>
                        <td><?= $s['size'] ?></td>
                        <td><?= $s['stok_keluar'] ?></td>



                        <?php if ($s['status'] == 0) : ?>
                            <?php if ($user['role_id'] == 2) : ?>
                                <td>

                                    <a href="<?= base_url('masterpermintaan/update/' . $s['kode_transaksi']) ?>" class="badge badge-success">Accept</a>
                                    <a href="<?= base_url('masterpermintaan/pdf/') . $s['kode_transaksi'] ?>" class="badge badge-primary">Print Label</a>

                                </td>
                            <?php elseif ($user['role_id'] == 4) : ?>
                                <td>
                                    <a href="#" class="badge badge-warning" data-toggle="modal" data-target="#editMenuModal<?= $s['kode_transaksi']; ?>">Edit</a>
                                </td>
                            <?php elseif ($user['role_id'] == 1) : ?>
                                <td>
                                    <a href="#" class="badge badge-warning" data-toggle="modal" data-target="#editMenuModal<?= $s['kode_transaksi']; ?>">Edit</a>
                                    <a href="<?= base_url('masterpermintaan/pdf/') . $s['kode_transaksi'] ?>" class="badge badge-primary">Print Label</a>
                                </td>
                            <?php endif; ?>
                        <?php else : ?>
                            <td>
                                <p class="badge badge-primary">Sudah diambil</p>
                            </td>
                        <?php endif; ?>
                    </tr>

                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

</div>

<div class="modal fade" id="newMenuModal" tabindex="-1" role="dialog" aria-labelledby="newMenuModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newMenuModalLabel">Tambah Permintaan Barang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('masterpermintaan') ?>" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="kode_transaksi">Kode Transaksi</label>
                        <input type="text" class="form-control" id="kode_transaksi" name="kode_transaksi" value="<?= $transaksi_max; ?>" placeholder="Kode Transaksi" readonly>
                    </div>
                    <div class="form-group">
                        <label for="line">Line</label>
                        <select name="line" id="line" class="form-control">
                            <option value="" selected disabled>Pilih Line</option>
                            <?php
                            for ($i = 1; $i <= 10; $i++) {
                            ?>
                                <option value="<?= $i; ?>"><?= $i; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="barang_id">Nama Barang</label>
                        <select name="barang_id" id="barang_id" class="form-control">
                            <option value="" selected disabled>Pilih Barang</option>
                            <?php foreach ($barang as $b) : ?>
                                <option value="<?= $b['id']; ?>"><?= $b['nama_barang']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="gender_id">Gender</label>
                        <select name="gender_id" id="gender_id" class="form-control">
                            <option value="" selected disabled>Pilih Gender</option>
                            <?php foreach ($gender as $g) : ?>
                                <option value="<?= $g['id']; ?>"><?= $g['gender']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="size">Size</label>
                        <select name="size" id="size" class="form-control">
                            <option value="" selected disabled>Pilih Size</option>
                            <?php
                            for ($i = 3; $i <= 15; $i++) {
                            ?>
                                <option value="<?= $i; ?>"><?= $i; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="style_id">Style</label>
                        <select name="style_id" id="style_id" class="form-control">
                            <option value="" selected disabled>Pilih Style</option>
                            <?php foreach ($style as $s) : ?>
                                <option value="<?= $s['id']; ?>"><?= $s['nama_style']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="stok_keluar">Jumlah Permintaan</label>
                        <input type="number" class="form-control" id="stok_keluar" name="stok_keluar" placeholder="Jumlah Permintaan" required>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Order</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php $i = 0;
foreach ($stock as $s) : $i++; ?>
    <div class="modal fade" id="editMenuModal<?= $s['kode_transaksi']; ?>" tabindex="-1" role="dialog" aria-labelledby="editMenuModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editMenuModalLabel">Edit Permintaan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?= base_url('masterpermintaan/updatePermintaan/' . $s['kode_transaksi']) ?>" method="post">
                    <input type="hidden" name="id" value="<?= $s['kode_transaksi']; ?>">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="kode_transaksi">Kode Transaksi</label>
                            <input type="text" class="form-control" id="kode_transaksi" name="kode_transaksi" value="<?= $s['kode_transaksi'] ?>" placeholder="Kode Transaksi" readonly>
                        </div>
                        <div class="form-group">
                            <label for="line">Line</label>
                            <select name="line" id="line" class="form-control">
                                <option value="" selected disabled>Pilih Line</option>
                                <?php
                                for ($i = 1; $i <= 10; $i++) {
                                ?>
                                    <?php if ($i == $s['line']) : ?>
                                        <option value="<?= $i; ?>" selected><?= $i; ?></option>
                                    <?php else : ?>
                                        <option value="<?= $i; ?>"><?= $i; ?></option>
                                    <?php endif; ?>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="barang_id">Nama Barang</label>
                            <select name="barang_id" id="barang_id" class="form-control">
                                <option value="" selected disabled>Barang</option>
                                <?php foreach ($barang as $b) : ?>
                                    <?php if ($b['id'] == $s['barang_id']) : ?>
                                        <option value="<?= $b['id']; ?>" selected><?= $b['nama_barang']; ?></option>
                                    <?php else : ?>
                                        <option value="<?= $b['id']; ?>"><?= $b['nama_barang']; ?></option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="gender_id">Gender</label>
                            <select name="gender_id" id="gender_id" class="form-control">
                                <option value="" selected disabled>Pilih Gender</option>
                                <?php foreach ($gender as $g) : ?>
                                    <?php if ($g['id'] == $s['gender_id']) : ?>
                                        <option value="<?= $g['id']; ?>" selected><?= $g['gender']; ?></option>
                                    <?php else : ?>
                                        <option value="<?= $g['id']; ?>"><?= $g['gender']; ?></option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="size">Size</label>
                            <select name="size" id="size" class="form-control">
                                <option value="" selected disabled>Pilih Size</option>
                                <?php
                                for ($i = 3; $i <= 15; $i++) {
                                ?>
                                    <?php if ($i == $s['size']) : ?>
                                        <option value="<?= $i; ?>" selected><?= $i; ?></option>
                                    <?php else : ?>
                                        <option value="<?= $i; ?>"><?= $i; ?></option>
                                    <?php endif; ?>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="style_id">Style</label>
                            <select name="style_id" id="style_id" class="form-control">
                                <option value="" selected disabled>Pilih Style</option>
                                <?php foreach ($style as $ss) : ?>
                                    <?php if ($ss['id'] == $s['style_id']) : ?>
                                        <option value="<?= $ss['id']; ?>" selected><?= $ss['nama_style']; ?></option>
                                    <?php else : ?>
                                        <option value="<?= $ss['id']; ?>"><?= $ss['nama_style']; ?></option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="stok_keluar">Jumlah Permintaan</label>
                            <input type="number" class="form-control" id="stok_keluar" name="stok_keluar" value="<?= $s['stok_keluar'] ?>" placeholder="Stock Barang" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php endforeach; ?>