<div class="container-fluid">
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
</div>

<div class="card">
    <div class="col-lg-6">

        <?= form_error(
            'menu',
            '<div class="alert alert-danger" role="alert">',
            '</div>'
        ); ?>

        <?= $this->session->flashdata('message'); ?>
    </div>
    <div class="card-body">
    <?php if($user['role_id'] == 2 || $user['role_id'] == 1) :?>
        <a href="" class="btn btn-primary mb-3" data-toggle="modal" data-target="#newMenuModal"> Tambah Barang Baru </a>
    <?php endif;?>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Kode Barang</th>
                    <th>Nama Barang</th>
                    <th>Stock</th>
                    <?php if($user['role_id'] == 2 || $user['role_id'] == 1) :?>
                    <th>Aksi</th>
                    <?php endif; ?>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                $kode = '12345';
                foreach ($barang as $b) : ?>
                    <tr>
                        <td><?= $no++; ?></td>
                        <td><?php
                            require 'vendor/autoload.php';
                            $generator = new Picqer\Barcode\BarcodeGeneratorPNG();
                            echo '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($b['kode_barang'], $generator::TYPE_CODE_128)) . '">';

                            ?></td>
                        <td><?= $b['nama_barang'] ?></td>
                        <td><?= $b['stock'] ?></td>
                        <?php if($user['role_id'] == 2 || $user['role_id'] == 1) :?>
                        <td>
                            <a href="#" class="badge badge-primary" data-toggle="modal" data-target="#barcodeModal<?= $b['kode_barang'] ?>">Barcode</a>
                            <a href="#" class="badge badge-warning" data-toggle="modal" data-target="#editMenuModal<?= $b['id']; ?>">Edit</a>
                            <a href="<?= base_url('masterbarang/delete/') . $b['id']; ?>" onclick="return confirm('Anda yakin ingin menghapus data ?')" class="badge badge-danger">Delete</a>
                        </td>
                        <?php endif;?>
                    </tr>
                    <div class="modal fade" id="barcodeModal<?= $b['kode_barang'] ?>" tabindex="-1" role="dialog" aria-labelledby="barcodeModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="barcodeModalLabel">Tambah Barang Baru</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="text-center">
                                        <?php
                                        require 'vendor/autoload.php';
                                        $generator = new Picqer\Barcode\BarcodeGeneratorPNG();
                                        echo '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($b['kode_barang'], $generator::TYPE_CODE_128)) . '">';

                                        ?>

                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

</div>

<div class="modal fade" id="newMenuModal" tabindex="-1" role="dialog" aria-labelledby="newMenuModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newMenuModalLabel">Tambah Barang Baru</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('masterbarang') ?>" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="kode_barang">Kode Barang</label>
                        <input type="text" class="form-control" id="kode_barang" name="kode_barang" value="<?= $transaksi_max ?>" placeholder="Kode Barang">
                    </div>
                    <div class="form-group">
                        <label for="nama_barang">Nama Barang</label>
                        <input type="text" class="form-control" id="nama_barang" name="nama_barang" placeholder="Nama Barang">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php $i = 0;
foreach ($barang as $b) : $i++; ?>
    <div class="modal fade" id="editMenuModal<?= $b['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="editMenuModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editMenuModalLabel">Edit User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?= base_url('masterbarang/update/' . $b['id']) ?>" method="post">
                    <input type="hidden" name="id" value="<?= $b['id']; ?>">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="nama_barang">Nama Barang</label>
                            <input type="text" class="form-control" id="nama_barang" value="<?= $b['nama_barang']; ?>" name="nama_barang" placeholder="Nama Barang">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php endforeach; ?>