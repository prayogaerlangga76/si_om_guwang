<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;
class Masterstock extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Barang_model');
        $this->load->model('Stock_model');
    }
    public function index()
    {
        $data['title'] = 'Master Stok';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
        $data['barang'] = $this->Barang_model->dataBarang();
        $data['stock'] = $this->Stock_model->dataStock();
        $data['style'] = $this->Stock_model->dataStyle();
        $data['gender'] = $this->Stock_model->dataGender();
        $max = $this->Stock_model->getMax();
        $str = str_pad($max + 1, 2, '0', STR_PAD_LEFT);
        $date = date('Ymd');
        $data['transaksi_max'] = $date . $str;
        $this->form_validation->set_rules('barang_id', 'Nama Barang', 'required');
        $this->form_validation->set_rules('stock_masuk', 'Stock Masuk', 'required');
        $this->form_validation->set_rules('kode_transaksi', 'Kode Transaksi', 'required');
        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('barang/stok', $data);
            $this->load->view('templates/footer');
        } else {
            $data = [
                'barang_id' => $this->input->post('barang_id'),
                'style_id' => $this->input->post('style_id'),
                'gender_id' => $this->input->post('gender_id'),
                'line' => $this->input->post('line'),
                'size' => $this->input->post('size'),
                'stok_masuk' => $this->input->post('stock_masuk'),
                'stok_keluar' => 0,
                'kode_transaksi' => $this->input->post('kode_transaksi'),
                'tanggal' => date('Y-m-d H:i:s'),
                'user_id' => $data['user']['id'],
                'tgl_scan' => date('Y-m-d H:i:s'),
                'tgl_exp' => date('Y-m-d H:i:s', strtotime('+14 days', strtotime(date('Y-m-d H:i:s')))),
            ];
            
            $this->db->insert('t_transaksi', $data);
            $this->Barang_model->updateStock($data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Stock masuk berhasil ditambahkan berhasil ditambahkan
          </div>');
            redirect('masterstock');
        }
    }

    public function laporan()
    {
        $data['title'] = 'Laporan';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
        $data['barang'] = $this->Barang_model->dataBarang();
        $data['stock'] = $this->Stock_model->dataStock();
        $max = $this->Stock_model->getMax();
        $str = str_pad($max + 1, 2, '0', STR_PAD_LEFT);
        $date = date('Ymd');
        $data['transaksi_max'] = $date . $str;
        // $query = "SELECT sum(stok_masuk) as stok_masuk, sum(stok_keluar) as stok_keluar FROM t_transaksi JOIN `t_barang` ON `t_transaksi`.`barang_id` = `t_barang`.`id` GROUP BY barang_id, tgl_scan";
        // $data['laporan'] = $this->db->query($query)->result_array();
        $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('barang/laporan', $data);
            $this->load->view('templates/footer');
    }
    public function update($id){
        $data = [
            'barang_id' => $this->input->post('barang_id'),
            'stok_masuk' => $this->input->post('stock_masuk'),
            'kode_transaksi' => $this->input->post('kode_transaksi'),
            'tanggal' => date('Y-m-d H:i:s'),
            'id' => $this->input->post('id'),
        ];
        $select = "SELECT stok_masuk FROM t_transaksi WHERE kode_transaksi= $data[kode_transaksi]";
        $return = $this->db->query($select)->row_array();
        $edit = [
            'stok_masuk' => $return['stok_masuk']
        ];
        $this->Barang_model->stockEdit($edit, $data);
        $this->Stock_model->updateData($id, $data);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Stock berhasil diubah
          </div>');
        redirect('masterstock');
    }
    public function getstock($id)
    {
        $data = $this->db->get_where('t_barang', ['kode_barang' => $id])->row_array();
        echo json_encode($data);
    }

}