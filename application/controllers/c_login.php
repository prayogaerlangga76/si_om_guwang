<?php
defined('BASEPATH') or exit('No direct script access allowed');

class c_login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('m_login');
    
    }

    public function login()
    {
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        if ($this->form_validation->run() == false) {
            $data['title'] = 'Halaman Login';
            $this->load->view('templates/auth_header', $data);
            $this->load->view('login/v_login');
            $this->load->view('templates/auth_footer');
        } else {
            // Validasi success
            $this->validasi();
        }
    }

    private function validasi()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $user = $this->m_login->cek_login($username);
        if ($user) {
            if (password_verify($password, $user['password'])) {
                $data = [
                    'username' => $user['username'],
                    'role_id' => $user['role_id']
                ];
                $this->session->set_userdata($data);
                if ($user['role_id'] == 1 || $user['role_id'] == 2) {

                    redirect('c_dashboard');
                } else {
                    redirect('user/dashboard');
                }
            }
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Username atau Password tidak ada
          </div>');
            redirect('c_login/login');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
            Akun belum terdaftar
          </div>');
            redirect('c_login/login');
        }
    }


    public function logout()
    {
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('role_id');
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Anda telah keluar dari sistem
          </div>');
        redirect('c_login/login');
    }
}
