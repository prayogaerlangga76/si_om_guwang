<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Masterbarang extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Barang_model');
        $this->load->library('Zend');
        $this->load->library('Ciqrcode');
        $this->zend->load('zend/barcode');
    }
    public function index()
    {
        $data['title'] = 'Master Barang';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
        $data['barang'] = $this->Barang_model->dataBarang();
        $max = $this->Barang_model->getMax();
        $str = str_pad($max + 1, 2, '0', STR_PAD_LEFT);
        $date = date('is');
        $data['transaksi_max'] = $date . $str;
        $this->form_validation->set_rules('nama_barang', 'Nama Barang', 'required');
        $this->form_validation->set_rules('kode_barang', 'Kode Barang', 'required');
        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('barang/index', $data);
            $this->load->view('templates/footer');
        } else {
            $data = [
                'nama_barang' => $this->input->post('nama_barang'),
                'kode_barang' => $this->input->post('kode_barang'),
                'stock' => 0,
            ];
            $this->db->insert('t_barang', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Barang berhasil ditambahkan
          </div>');
            redirect('masterbarang');
        }
    }
    public function QRcode($kodenya)
    {
        //render  qr code dengan format gambar PNG
        QRcode::png(
            $kodenya,
            $outfile = false,
            $level = QR_ECLEVEL_H,
            $size  = 6,
            $margin = 2
        );
    }

    public function Barcode($code)
    {
        require 'vendor/autoload.php';
        $generator = new Picqer\Barcode\BarcodeGeneratorPNG();
        echo '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($code, $generator::TYPE_CODE_128)) . '">';
    }


    public function update($id)
    {
        $data = [
            'nama_barang' => $this->input->post('nama_barang'),
            'id' => $this->input->post('id'),
        ];

        $this->Barang_model->updateData($id, $data);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Barang berhasil diubah
          </div>');
        redirect('masterbarang');
    }

    public function delete($id)
    {
        $this->Barang_model->deleteData($id);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Barang berhasil dihapus
          </div>');
        redirect('masterbarang');
    }
}
