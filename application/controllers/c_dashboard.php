<?php
defined('BASEPATH') or exit('No direct script access allowed');

class c_dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('m_kelola_pengguna');
    }
    public function index()
    {
        $data['title'] = 'Dashboard';
        $data['user'] = $this->m_kelola_pengguna->getUsername($this->session->userdata('username'));

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('v_dashboard/admin/index', $data);
        $this->load->view('templates/footer');
    }
}
