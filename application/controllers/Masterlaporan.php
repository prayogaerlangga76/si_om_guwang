<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;
class Masterlaporan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Laporan_model');
    }
    public function index(){
        $data['title'] = 'Master Laporan';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
        $data['laporan'] = $this->Laporan_model->getData();
        $data['permintaan'] = $this->Laporan_model->getDataPermintaan();
        $data['persediaan'] = $this->Laporan_model->getDataPersediaan();
        $data['pengguna'] = $this->Laporan_model->getDataPengguna();
        
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('laporan/index', $data);
            $this->load->view('templates/footer');
        
    }

}