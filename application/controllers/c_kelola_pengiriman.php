<?php
defined('BASEPATH') or exit('No direct script access allowed');

class c_kelola_pengiriman extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('m_kelola_pengguna');
        $this->load->model('Stock_model');
    }
    public function index()
    {
        $data['title'] = 'Kelola Pengiriman';
        $data['user'] = $this->m_kelola_pengguna->getUsername($this->session->userdata('username'));
        $data['stock'] = $this->Stock_model->dataPengiriman();
        $this->form_validation->set_rules('kode_transaksi', 'Kode Transaksi', 'required');
        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('pengiriman/index', $data);
            $this->load->view('templates/footer');
        } else {
            $data = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
            $kode = $this->db->get_where('t_transaksi', ['kode_transaksi' => $this->input->post('kode_transaksi')])->row_array();
            $query = "UPDATE t_transaksi SET status = 1, petugas_id = $data[id] WHERE kode_transaksi = $kode[kode_transaksi]";
            $barang = "UPDATE t_barang SET stock = stock - $kode[stok_keluar] WHERE id = $kode[barang_id]";
            $this->db->query($query);
            $this->db->query($barang);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Material sudah dikirim
          </div>');
            redirect('c_kelola_pengiriman');
        }
    }
}
