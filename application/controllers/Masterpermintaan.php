<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;
class Masterpermintaan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Barang_model');
        $this->load->model('Stock_model');
        $this->load->helper('date');
        $this->load->library ('Mcarbon'); 
    }

    public function index()
    {
        $data['title'] = 'Master Permintaan';
        $data['user'] = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
        $data['barang'] = $this->Barang_model->dataBarang();
        $data['stock'] = $this->Stock_model->dataStockPermintaan();
        $data['style'] = $this->Stock_model->dataStyle();
        $data['gender'] = $this->Stock_model->dataGender();
        $max = $this->Stock_model->getMax();
        $str = str_pad($max + 1, 2, '0', STR_PAD_LEFT);
        $date = date('Ymd');
        $data['transaksi_max'] = $date . $str;
        $this->form_validation->set_rules('barang_id', 'Nama Barang', 'required');
        $this->form_validation->set_rules('stok_keluar', 'Stock Keluar', 'required');
        $this->form_validation->set_rules('kode_transaksi', 'Kode Transaksi', 'required');
        $this->form_validation->set_rules('line', 'Line', 'required');
        $this->form_validation->set_rules('size', 'Size', 'required');
        $this->form_validation->set_rules('style_id', 'Style', 'required');
        $this->form_validation->set_rules('gender_id', 'Gender', 'required');
        if ($this->form_validation->run() == false) {
            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('barang/permintaan', $data);
            $this->load->view('templates/footer');
        } else {
            $data = [
                'barang_id' => $this->input->post('barang_id'),
                'style_id' => $this->input->post('style_id'),
                'gender_id' => $this->input->post('gender_id'),
                'line' => $this->input->post('line'),
                'size' => $this->input->post('size'),
                'stok_masuk' => 0,
                'stok_keluar' => $this->input->post('stok_keluar'),
                'kode_transaksi' => $this->input->post('kode_transaksi'),
                'status' => 0,
                'admin_id' => $data['user']['id'],
                'tanggal' => date('Y-m-d H:i:s'),
                'user_id' => $data['user']['id'],
                'tgl_scan' => date('Y-m-d H:i:s'),
                'tgl_exp' => date('Y-m-d H:i:s', strtotime('+14 days', strtotime(date('Y-m-d H:i:s')))),
            ];
            $this->db->insert('t_transaksi', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Permintaan barang telah dikirim ke gudang
          </div>');
            redirect('masterpermintaan');
        }
    }
    public function updatePermintaan($id){
        $data = [
            'barang_id' => $this->input->post('barang_id'),
            'style_id' => $this->input->post('style_id'),
            'gender_id' => $this->input->post('gender_id'),
            'line' => $this->input->post('line'),
            'size' => $this->input->post('size'),
            'stok_keluar' => $this->input->post('stok_keluar'),
            'kode_transaksi' => $this->input->post('kode_transaksi'),
            'status' => 0,
        ];
        $this->Barang_model->updatePermintaan($id, $data);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
        Permintaan telah diubah
      </div>');
        redirect('masterpermintaan');
    }

    public function update($id)
    {
        $data = $this->db->get_where('user', ['username' => $this->session->userdata('username')])->row_array();
        $kode = $this->db->get_where('t_transaksi', ['kode_transaksi' => $id])->row_array();
        $query = "UPDATE t_transaksi SET status = 1, petugas_id = $data[id] WHERE kode_transaksi = $kode[kode_transaksi]";
        $barang = "UPDATE t_barang SET stock = stock - $kode[stok_keluar] WHERE id = $kode[barang_id]";
        $this->db->query($query);
        $this->db->query($barang);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Barang Sudah diberikan
          </div>');
            redirect('masterpermintaan');
    }
    public function pdf($id)
	{

		$this->load->library('dompdf_gen');
        $data['transaksi'] = $this->Stock_model->print($id);
        $this->load->view('laporan/printlabel', $data);
        $paper_size = array(0,0,480,480);
        $orientation = 'landscape';
        $html = $this->output->get_output();
        $this->dompdf->set_paper($paper_size, $orientation);

        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream('label_print.pdf', array('attachment' => 0));
	}
}