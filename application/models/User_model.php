<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends CI_Model
{
    public function getDataUser()
    {
        $query = "SELECT `user`.`id`, `user`.`nama`, `user`.`username` , `user`.`role_id`, `user_role`.`role` FROM `user` JOIN `user_role` ON `user` . `role_id` = `user_role` . `id`";

        return $this->db->query($query)->result_array();
    }

    public function getDataRole()
    {
        $query = "SELECT * FROM user_role";

        return $this->db->query($query)->result_array();
    }

    public function updateData($id, $data)
    {
        $query = "UPDATE user SET nama = '$data[nama]', username='$data[username]', password = '$data[password]', role_id = '$data[role_id]' WHERE id = $id";
        return $this->db->query($query);
    }

    public function deleteData($id)
    {
        $query = "DELETE FROM user WHERE id = $id";
        return $this->db->query($query);
    }
}