<?php
defined('BASEPATH') or exit('No direct script access allowed');

class m_login extends CI_Model
{
    public function cek_login($username){

        return $this->db->get_where('user', ['username' => $username])->row_array();
    }
}
