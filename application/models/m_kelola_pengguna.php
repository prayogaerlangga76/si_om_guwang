<?php
defined('BASEPATH') or exit('No direct script access allowed');

class m_kelola_pengguna extends CI_Model
{
    public function getUsername($username){

        return $this->db->get_where('user', ['username' => $username])->row_array();
    }
}