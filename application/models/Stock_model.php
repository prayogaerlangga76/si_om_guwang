<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Stock_model extends CI_Model
{
    public function dataStock()
    {
        $query = "SELECT `t_transaksi`.`id`, `t_transaksi`.`kode_transaksi`, `t_barang`.`nama_barang`, `user`.`nama`,`t_transaksi`.`stok_masuk`, `t_transaksi`.`tgl_scan`, `t_transaksi`.`tgl_exp` , `t_style`.`nama_style`, `t_transaksi`.`size`, `t_gender`.`gender` FROM `t_transaksi` JOIN `t_barang` ON `t_transaksi` . `barang_id` = `t_barang` . `id` JOIN `user` ON `t_transaksi`.`user_id` = `user`.`id` JOIN `t_style` ON `t_transaksi`.`style_id` = `t_style`.`id` JOIN `t_gender` ON `t_transaksi`.`gender_id` = `t_gender`.`id` WHERE stok_masuk > 0";
        return $this->db->query($query)->result_array();
    }
    public function print($id)
    {
        $query = "SELECT `t_transaksi`.`id`, `t_transaksi`.`kode_transaksi`, `t_barang`.`nama_barang`, `user`.`nama`, `t_transaksi`.`stok_keluar`, `t_transaksi`.`status`, `t_style`.`nama_style`, `t_gender`.`gender`, `t_transaksi`.`line`, `t_transaksi`.`size` FROM `t_transaksi` JOIN `t_barang` ON `t_transaksi` . `barang_id` = `t_barang` . `id` JOIN `user` ON `t_transaksi`.`admin_id` = `user`.`id` JOIN `t_style` ON `t_transaksi`.`style_id` = `t_style`.`id` JOIN `t_gender` ON `t_transaksi`.`gender_id` = `t_gender`.`id` WHERE kode_transaksi = $id";
        return $this->db->query($query)->row_array();
    }
    public function dataStyle()
    {
        $query = "SELECT * FROM t_style";
        return $this->db->query($query)->result_array();
    }
    public function dataGender()
    {
        $query = "SELECT * FROM t_gender";
        return $this->db->query($query)->result_array();
    }
    public function dataStockPermintaan()
    {
        $query = "SELECT `t_transaksi`.`id`, `t_transaksi`.`kode_transaksi`, `t_barang`.`nama_barang`, `user`.`nama`, `t_transaksi`.`stok_keluar`, `t_transaksi`.`status`, `t_style`.`nama_style`, `t_gender`.`gender`, `t_transaksi`.`barang_id`, `t_transaksi`.`line`, `t_transaksi`.`size`, `t_transaksi`.`gender_id`, `t_transaksi`.`style_id` FROM `t_transaksi` JOIN `t_barang` ON `t_transaksi` . `barang_id` = `t_barang` . `id` JOIN `user` ON `t_transaksi`.`admin_id` = `user`.`id` JOIN `t_style` ON `t_transaksi`.`style_id` = `t_style`.`id` JOIN `t_gender` ON `t_transaksi`.`gender_id` = `t_gender`.`id` WHERE stok_keluar > 0";
        return $this->db->query($query)->result_array();
    }
    public function dataPengiriman()
    {
        $query = "SELECT `t_transaksi`.`id`, `t_transaksi`.`kode_transaksi`, `t_barang`.`nama_barang`, `user`.`nama`, `t_transaksi`.`stok_keluar`, `t_transaksi`.`status`, `t_style`.`nama_style`, `t_gender`.`gender`, `t_transaksi`.`barang_id`, `t_transaksi`.`line`, `t_transaksi`.`size` FROM `t_transaksi` JOIN `t_barang` ON `t_transaksi` . `barang_id` = `t_barang` . `id` JOIN `user` ON `t_transaksi`.`admin_id` = `user`.`id` JOIN `t_style` ON `t_transaksi`.`style_id` = `t_style`.`id` JOIN `t_gender` ON `t_transaksi`.`gender_id` = `t_gender`.`id` WHERE status = 1";
        return $this->db->query($query)->result_array();
    }
    public function data()
    {
        $query = "SELECT `t_transaksi`.`id`, `t_transaksi`.`kode_transaksi`, `t_barang`.`nama_barang`, `user`.`nama`,`t_transaksi`.`stok_keluar` FROM `t_transaksi` JOIN `t_barang` ON `t_transaksi` . `barang_id` = `t_barang` . `id` JOIN `user` ON `t_transaksi`.`admin_id` = `user`.`id` WHERE stok_keluar > 0";
        
        return $this->db->query($query)->result_array();
    }

    public function updateData($id, $data)
    {
        $query = "UPDATE t_transaksi SET barang_id = $data[barang_id], stok_masuk = $data[stok_masuk] WHERE kode_transaksi = '$data[kode_transaksi]';";
        return $this->db->query($query);
    }
    
    public function deleteData($id)
    {
        $query = "DELETE FROM t_transaksi WHERE kode_transaksi = $id";
        return $this->db->query($query);
    }
    public function getMax()
    {
        return $this->db->count_all_results('t_transaksi');
    }
}