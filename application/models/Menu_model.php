<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu_model extends CI_Model
{
    public function getSubMenu()
    {
        $query = "SELECT `user_sub_menu`.*, `user_menu`.`menu` FROM `user_sub_menu` JOIN `user_menu` ON
        `user_sub_menu`.`menu_id` =  `user_menu`.`id`";
        return $this->db->query($query)->result_array();
    }

    public function updateData($id, $data)
    {
        $query = "UPDATE user_menu SET menu = '$data' WHERE id = $id";
        return $this->db->query($query);
    }
    public function updateSubMenuData($id, $data)
    {
        $query = "UPDATE user_sub_menu SET menu_id = '$data[menu_id]', title = '$data[title]', url = '$data[url]', icon = '$data[icon]', is_active = '$data[is_active] == 1 ? 1 : 0' WHERE id = $id";
        return $this->db->query($query);
    }

    public function deleteData($id)
    {
        $query = "DELETE FROM user_menu WHERE id = $id";
        return $this->db->query($query);
    }
    public function deleteSubMenuData($id)
    {
        $query = "DELETE FROM user_sub_menu WHERE id = $id";
        return $this->db->query($query);
    }
}
