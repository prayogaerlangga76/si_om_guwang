<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Barang_model extends CI_Model
{
    public function dataBarang()
    {
        $query = "SELECT * FROM t_barang";
        return $this->db->query($query)->result_array();
    }

    public function updateData($id, $data)
    {
        $query = "UPDATE t_barang SET nama_barang = '$data[nama_barang]' WHERE id = $id";
        return $this->db->query($query);
    }
    public function updateStock($data)
    {
        $query = "UPDATE t_barang SET stock = stock + $data[stok_masuk] WHERE id = $data[barang_id]";
        return $this->db->query($query);
    }
    public function updatePermintaan($id, $data)
    {
        // $this->db->set('barang_id', $data['barang_id']);
        // $this->db->set('style_id', $data['style_id']);
        // $this->db->set('line', $data['line']);
        // $this->db->set('size', $data['size']);
        // $this->db->set('stok_masuk', $data['stok_masuk']);
        // $this->db->set('stok_keluar', $data['stok_keluar']);
        // $this->db->set('status', $data['status']);
        // $this->db->where('kode_transaksi', $data['kode_transaksi']);
        // $query = $this->db->update('t_transaksi');
        // var_dump($data);
        // die;
        $query = "UPDATE t_transaksi SET barang_id = $data[barang_id], style_id = $data[style_id], line = $data[line], size = $data[size], stok_keluar = $data[stok_keluar], status = $data[status], gender_id = $data[gender_id] WHERE kode_transaksi = $id  ";
        return $this->db->query($query);
    }
    public function stockEdit($edit, $data)
    {
        $query = "UPDATE t_barang SET stock = (stock - $edit[stok_masuk]) + $data[stok_masuk] WHERE id = $data[barang_id]";
        return $this->db->query($query);
    }
    public function stockDelete($delete)
    {
        $query = "UPDATE t_barang SET stock = stock - $delete[stok_masuk] WHERE id = $delete[barang_id]";
        return $this->db->query($query);
    }

    public function deleteData($id)
    {
        $query = "DELETE FROM t_barang WHERE id = $id";
        return $this->db->query($query);
    }
    public function getMax()
    {
        return $this->db->count_all_results('t_barang');
    }
}
