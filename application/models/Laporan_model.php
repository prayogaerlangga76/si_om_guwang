<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Laporan_model extends CI_Model
{
    public function getData(){
        $query = "SELECT  t_barang.nama_barang, DATE(tgl_scan) AS tgl_scan, DATE(tgl_exp) AS tgl_exp, SUM(stok_masuk) - SUM(stok_keluar) AS sisa_stok FROM t_transaksi JOIN t_barang ON t_barang.id = t_transaksi.barang_id GROUP BY barang_id, DATE(tgl_exp)";
        return $this->db->query($query)->result_array();
    }
    public function getDataPengguna(){
        $query = "SELECT `user`.`id`, `user`.`nama`, `user`.`username` , `user`.`role_id`, `user_role`.`role` FROM `user` JOIN `user_role` ON `user` . `role_id` = `user_role` . `id`";

        return $this->db->query($query)->result_array();
    }
    public function getDataPermintaan(){
        $query = "SELECT `t_transaksi`.`id`, `t_transaksi`.`kode_transaksi`, `t_barang`.`nama_barang`, `user`.`nama`, `t_transaksi`.`stok_keluar`, `t_transaksi`.`status`, `t_style`.`nama_style`, `t_gender`.`gender`, `t_transaksi`.`barang_id`, `t_transaksi`.`line`, `t_transaksi`.`size` FROM `t_transaksi` JOIN `t_barang` ON `t_transaksi` . `barang_id` = `t_barang` . `id` JOIN `user` ON `t_transaksi`.`admin_id` = `user`.`id` JOIN `t_style` ON `t_transaksi`.`style_id` = `t_style`.`id` JOIN `t_gender` ON `t_transaksi`.`gender_id` = `t_gender`.`id` WHERE stok_keluar > 0";
        return $this->db->query($query)->result_array();
    }
    public function getDataPersediaan(){
        $query = "SELECT `t_transaksi`.`id`, `t_transaksi`.`kode_transaksi`, `t_barang`.`nama_barang`, `user`.`nama`,`t_transaksi`.`stok_masuk`, `t_transaksi`.`tgl_scan`, `t_transaksi`.`tgl_exp` , `t_style`.`nama_style`, `t_transaksi`.`size`, `t_gender`.`gender` FROM `t_transaksi` JOIN `t_barang` ON `t_transaksi` . `barang_id` = `t_barang` . `id` JOIN `user` ON `t_transaksi`.`user_id` = `user`.`id` JOIN `t_style` ON `t_transaksi`.`style_id` = `t_style`.`id` JOIN `t_gender` ON `t_transaksi`.`gender_id` = `t_gender`.`id` WHERE stok_masuk > 0";
        return $this->db->query($query)->result_array();
    }
}
