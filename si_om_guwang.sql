-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.33 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table kpspam.bulan
CREATE TABLE IF NOT EXISTS `bulan` (
  `id_bulan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_bulan` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_bulan`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Dumping data for table kpspam.bulan: ~12 rows (approximately)
/*!40000 ALTER TABLE `bulan` DISABLE KEYS */;
INSERT INTO `bulan` (`id_bulan`, `nama_bulan`) VALUES
	(1, 'Januari'),
	(2, 'Febuari'),
	(3, 'Maret'),
	(4, 'April'),
	(5, 'Mei'),
	(6, 'Juni'),
	(7, 'Juli'),
	(8, 'Agustus'),
	(9, 'September'),
	(10, 'Oktober'),
	(11, 'November'),
	(12, 'Desember');
/*!40000 ALTER TABLE `bulan` ENABLE KEYS */;

-- Dumping structure for table kpspam.pelanggan
CREATE TABLE IF NOT EXISTS `pelanggan` (
  `id_pelanggan` int(11) NOT NULL AUTO_INCREMENT,
  `rek_pelanggan` varchar(128) NOT NULL,
  `nama_pelanggan` varchar(128) NOT NULL,
  `alamat` text NOT NULL,
  `no_telepon` varchar(50) NOT NULL DEFAULT '',
  `email` varchar(50) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `tarif_id` int(11) DEFAULT NULL,
  `is_active` int(1) DEFAULT NULL,
  PRIMARY KEY (`id_pelanggan`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

-- Dumping data for table kpspam.pelanggan: ~1 rows (approximately)
/*!40000 ALTER TABLE `pelanggan` DISABLE KEYS */;
INSERT INTO `pelanggan` (`id_pelanggan`, `rek_pelanggan`, `nama_pelanggan`, `alamat`, `no_telepon`, `email`, `user_id`, `created_at`, `tarif_id`, `is_active`) VALUES
	(26, '20220400001', 'Prayoga Erlangga Putra', 'akamslckamlck', '09809809', 'prayoga@gmail.com', 7, 1650172874, 1, 1);
/*!40000 ALTER TABLE `pelanggan` ENABLE KEYS */;

-- Dumping structure for table kpspam.pemakaian
CREATE TABLE IF NOT EXISTS `pemakaian` (
  `id_pemakaian` int(128) NOT NULL AUTO_INCREMENT,
  `no_pembayaran` varchar(50) DEFAULT NULL,
  `pelanggan_id` int(11) unsigned NOT NULL,
  `bulan_id` int(11) NOT NULL,
  `tahun` int(11) NOT NULL,
  `meter_bulan_lalu` int(11) NOT NULL DEFAULT '0',
  `meter_bulan_ini` int(11) NOT NULL,
  `total_pemakaian` int(11) NOT NULL,
  `total_tagihan` int(11) DEFAULT NULL,
  `tgl_tagihan` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_pemakaian`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

-- Dumping data for table kpspam.pemakaian: ~2 rows (approximately)
/*!40000 ALTER TABLE `pemakaian` DISABLE KEYS */;
INSERT INTO `pemakaian` (`id_pemakaian`, `no_pembayaran`, `pelanggan_id`, `bulan_id`, `tahun`, `meter_bulan_lalu`, `meter_bulan_ini`, `total_pemakaian`, `total_tagihan`, `tgl_tagihan`) VALUES
	(41, 'PM20220400001', 26, 1, 2022, 0, 0, 0, NULL, NULL),
	(42, 'PM2022041700002', 26, 3, 2022, 0, 20, 20, 40000, 1650172931);
/*!40000 ALTER TABLE `pemakaian` ENABLE KEYS */;

-- Dumping structure for table kpspam.tagihan
CREATE TABLE IF NOT EXISTS `tagihan` (
  `id_tagihan` int(11) NOT NULL AUTO_INCREMENT,
  `no_pembayaran` varchar(50) NOT NULL,
  `jumlah_tagihan` int(11) NOT NULL,
  `bayar` int(11) DEFAULT NULL,
  `kembalian` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `image` varchar(50) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `pelanggan_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_tagihan`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table kpspam.tagihan: ~1 rows (approximately)
/*!40000 ALTER TABLE `tagihan` DISABLE KEYS */;
INSERT INTO `tagihan` (`id_tagihan`, `no_pembayaran`, `jumlah_tagihan`, `bayar`, `kembalian`, `status`, `image`, `created_at`, `updated_at`, `pelanggan_id`) VALUES
	(11, 'PM2022041700002', 40000, 40000, 0, 1, NULL, 1650172931, 1650173153, 26);
/*!40000 ALTER TABLE `tagihan` ENABLE KEYS */;

-- Dumping structure for table kpspam.tarif_layanan
CREATE TABLE IF NOT EXISTS `tarif_layanan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(255) NOT NULL,
  `tarif` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table kpspam.tarif_layanan: ~1 rows (approximately)
/*!40000 ALTER TABLE `tarif_layanan` DISABLE KEYS */;
INSERT INTO `tarif_layanan` (`id`, `kategori`, `tarif`) VALUES
	(1, 'A', 2000);
/*!40000 ALTER TABLE `tarif_layanan` ENABLE KEYS */;

-- Dumping structure for table kpspam.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_active` int(1) NOT NULL,
  `date_created` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table kpspam.user: ~3 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `username`, `image`, `password`, `role_id`, `is_active`, `date_created`, `nama`) VALUES
	(1, 'prayogaep', 'default.jpg', '$2y$10$sSl0OhYhE6VU6LN9wXvaAuZV6iMKdv0HWjm/qaXPLuLNZyBojMepW', 1, 1, 1648126951, 'Prayoga Erlangga Putra'),
	(3, 'muksinalatas', 'default.jpg', '$2y$10$09BNltkJ8LUkax6XVyrjP.86TSwGfSdFgIbntJWnbqEdBnXhFLley', 3, 1, 1648127329, 'Muksin Alatas'),
	(7, 'prayoga2', 'default.jpg', '$2y$10$WF3U0cLNIeythHdWhy22reEAdyVOdh/Dp6GNYz3dFF/WugcFFGgG6', 3, 1, 1650172874, 'Prayoga Erlangga Putra');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- Dumping structure for table kpspam.user_akses_menu
CREATE TABLE IF NOT EXISTS `user_akses_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table kpspam.user_akses_menu: ~5 rows (approximately)
/*!40000 ALTER TABLE `user_akses_menu` DISABLE KEYS */;
INSERT INTO `user_akses_menu` (`id`, `role_id`, `menu_id`) VALUES
	(1, 1, 1),
	(2, 1, 3),
	(3, 2, 1),
	(4, 3, 3),
	(5, 1, 4);
/*!40000 ALTER TABLE `user_akses_menu` ENABLE KEYS */;

-- Dumping structure for table kpspam.user_menu
CREATE TABLE IF NOT EXISTS `user_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table kpspam.user_menu: ~3 rows (approximately)
/*!40000 ALTER TABLE `user_menu` DISABLE KEYS */;
INSERT INTO `user_menu` (`id`, `menu`) VALUES
	(1, 'Admin'),
	(3, 'Pelanggan'),
	(4, 'Menu');
/*!40000 ALTER TABLE `user_menu` ENABLE KEYS */;

-- Dumping structure for table kpspam.user_role
CREATE TABLE IF NOT EXISTS `user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table kpspam.user_role: ~3 rows (approximately)
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` (`id`, `role`) VALUES
	(1, 'Administrator'),
	(2, 'Staff'),
	(3, 'Pelanggan');
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;

-- Dumping structure for table kpspam.user_sub_menu
CREATE TABLE IF NOT EXISTS `user_sub_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `url` varchar(128) NOT NULL,
  `icon` varchar(128) NOT NULL,
  `is_active` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table kpspam.user_sub_menu: ~9 rows (approximately)
/*!40000 ALTER TABLE `user_sub_menu` DISABLE KEYS */;
INSERT INTO `user_sub_menu` (`id`, `menu_id`, `title`, `url`, `icon`, `is_active`) VALUES
	(1, 1, 'Dashboard', 'admin', 'fas fa-fw fa-tachometer-alt', 1),
	(4, 4, 'Menu Management', 'menu', 'fa-solid fa-fw fa-grip', 1),
	(5, 4, 'Sub Menu Management', 'menu/submenu', 'fa-solid fa-fw fa-bars', 1),
	(6, 1, 'Tarif Layanan', 'tarif', 'fa-solid fa-fw fa-coins', 1),
	(7, 1, 'Daftar Pelanggan', 'pelanggan', 'fa-solid fa-fw fa-users', 1),
	(8, 1, 'Data Pemakaian', 'pemakaian', 'fas fa-fw fa-tachometer-alt', 1),
	(9, 1, 'Data Tagihan', 'tagihan', 'fa-solid fa-fw fa-check', 1),
	(10, 3, 'Beranda', 'user/dashboard', 'fa-solid fa-fw fa-house-user', 1),
	(11, 3, 'Profile', 'user', 'fa-solid fa-fw fa-user', 1);
/*!40000 ALTER TABLE `user_sub_menu` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
