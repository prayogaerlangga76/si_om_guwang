-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.33 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for si_om_guwang
CREATE DATABASE IF NOT EXISTS `si_om_guwang` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `si_om_guwang`;

-- Dumping structure for table si_om_guwang.t_barang
CREATE TABLE IF NOT EXISTS `t_barang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_barang` varchar(50) NOT NULL,
  `stock` int(11) NOT NULL,
  `kode_barang` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- Dumping data for table si_om_guwang.t_barang: ~11 rows (approximately)
/*!40000 ALTER TABLE `t_barang` DISABLE KEYS */;
INSERT INTO `t_barang` (`id`, `nama_barang`, `stock`, `kode_barang`) VALUES
	(7, 'Air Max', 390, 394301),
	(8, 'Wafle One', 0, 395602),
	(9, 'Force One', 0, 400503),
	(10, 'Revolution', 0, 401504),
	(11, 'Down Shifter', 50, 402705),
	(12, 'Meller', 0, 404106),
	(13, 'Pegasus', 0, 404907),
	(14, 'Invigor', 0, 405508),
	(15, 'Court Borough', 0, 410109),
	(16, 'Alpa Trainer', 0, 411310),
	(17, 'Presto Ip', 0, 412111);
/*!40000 ALTER TABLE `t_barang` ENABLE KEYS */;

-- Dumping structure for table si_om_guwang.t_gender
CREATE TABLE IF NOT EXISTS `t_gender` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gender` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table si_om_guwang.t_gender: ~2 rows (approximately)
/*!40000 ALTER TABLE `t_gender` DISABLE KEYS */;
INSERT INTO `t_gender` (`id`, `gender`) VALUES
	(1, 'Women'),
	(2, 'Man');
/*!40000 ALTER TABLE `t_gender` ENABLE KEYS */;

-- Dumping structure for table si_om_guwang.t_style
CREATE TABLE IF NOT EXISTS `t_style` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- Dumping data for table si_om_guwang.t_style: ~21 rows (approximately)
/*!40000 ALTER TABLE `t_style` DISABLE KEYS */;
INSERT INTO `t_style` (`id`, `nama`) VALUES
	(1, 'DD-001'),
	(2, 'DD-100'),
	(3, 'DH-100'),
	(4, 'DH-001'),
	(5, 'DQ-001'),
	(6, 'DQ-100'),
	(7, 'DQ-002'),
	(8, 'DX-100'),
	(9, 'DX-001'),
	(10, 'CT-100'),
	(11, 'CT-001'),
	(12, 'DM-001'),
	(13, 'DM-100'),
	(14, 'DM-002'),
	(15, 'DJ-100'),
	(16, 'DJ-001'),
	(17, 'DZ-001'),
	(18, 'DZ-100'),
	(19, 'DZ-300'),
	(20, 'CI-001'),
	(21, 'CI-100');
/*!40000 ALTER TABLE `t_style` ENABLE KEYS */;

-- Dumping structure for table si_om_guwang.t_transaksi
CREATE TABLE IF NOT EXISTS `t_transaksi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `barang_id` int(11) NOT NULL,
  `stok_masuk` int(11) DEFAULT NULL,
  `stok_keluar` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `kode_transaksi` int(128) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `tanggal` timestamp NULL DEFAULT NULL,
  `petugas_id` int(11) DEFAULT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `tgl_scan` timestamp NULL DEFAULT NULL,
  `tgl_exp` timestamp NULL DEFAULT NULL,
  `gender_id` int(11) DEFAULT NULL,
  `line` int(11) DEFAULT NULL,
  `style_id` int(11) DEFAULT NULL,
  `Column 16` int(11) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table si_om_guwang.t_transaksi: ~7 rows (approximately)
/*!40000 ALTER TABLE `t_transaksi` DISABLE KEYS */;
INSERT INTO `t_transaksi` (`id`, `barang_id`, `stok_masuk`, `stok_keluar`, `user_id`, `kode_transaksi`, `status`, `tanggal`, `petugas_id`, `admin_id`, `tgl_scan`, `tgl_exp`, `gender_id`, `line`, `style_id`, `Column 16`, `size`) VALUES
	(1, 7, 200, 0, 1, 2022061601, NULL, '2022-06-16 15:48:22', NULL, NULL, '2022-06-16 15:48:22', '2022-06-30 15:48:22', 1, 1, 13, NULL, 14),
	(2, 8, 0, 20, NULL, 2022061602, 1, '2022-06-16 15:49:06', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 7, 90, 0, 1, 2022061703, NULL, '2022-06-17 22:22:24', NULL, NULL, '2022-06-17 22:22:24', '2022-07-01 22:22:24', 1, 2, 13, NULL, 12),
	(4, 8, 0, 90, NULL, 2022061704, 1, '2022-06-17 22:24:33', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(5, 8, 110, 0, 1, 2022061705, NULL, '2022-06-17 22:26:24', NULL, NULL, '2022-06-17 22:26:24', '2022-07-01 22:26:24', 1, 6, 13, NULL, 14),
	(6, 11, 100, 0, 1, 2022062006, NULL, '2022-06-20 13:25:33', NULL, NULL, '2022-06-20 13:25:33', '2022-07-04 13:25:33', 1, 3, 16, NULL, 11),
	(7, 11, 0, 50, NULL, 2022062007, 1, '2022-06-20 13:26:03', 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `t_transaksi` ENABLE KEYS */;

-- Dumping structure for table si_om_guwang.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_active` int(1) NOT NULL,
  `date_created` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumping data for table si_om_guwang.user: ~4 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `username`, `image`, `password`, `role_id`, `is_active`, `date_created`, `nama`) VALUES
	(1, 'root', 'default.jpg', '$2y$10$sSl0OhYhE6VU6LN9wXvaAuZV6iMKdv0HWjm/qaXPLuLNZyBojMepW', 1, 1, 1648126951, 'User Root'),
	(3, 'muksinalatas', 'default.jpg', '$2y$10$bGK0JWxtUuTss0cNhOTNz.nnq.341YYZ53KUbx3c/Rzz8r0MaNIBS', 3, 1, 1648127329, 'Muksin Alatas'),
	(8, 'septian', 'default.jpg', '$2y$10$ZMLDxqCNiEAoMeanIWqBk.MfhLb0h2xJTQiXWHRYLI1rkLE3ivOGu', 2, 1, 1654856953, 'Septian'),
	(9, 'prayoga', 'default.jpg', '$2y$10$2lfyWByvjKd4eFbeu7mqfOPEqqxiQXbGTHSTgc5tbImfhZqoyFFt.', 4, 1, 1648126951, 'Prayoga');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- Dumping structure for table si_om_guwang.user_akses_menu
CREATE TABLE IF NOT EXISTS `user_akses_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table si_om_guwang.user_akses_menu: ~7 rows (approximately)
/*!40000 ALTER TABLE `user_akses_menu` DISABLE KEYS */;
INSERT INTO `user_akses_menu` (`id`, `role_id`, `menu_id`) VALUES
	(1, 1, 1),
	(2, 1, 3),
	(3, 2, 3),
	(5, 1, 4),
	(6, 1, 5),
	(7, 4, 1),
	(8, 3, 5);
/*!40000 ALTER TABLE `user_akses_menu` ENABLE KEYS */;

-- Dumping structure for table si_om_guwang.user_menu
CREATE TABLE IF NOT EXISTS `user_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table si_om_guwang.user_menu: ~4 rows (approximately)
/*!40000 ALTER TABLE `user_menu` DISABLE KEYS */;
INSERT INTO `user_menu` (`id`, `menu`) VALUES
	(1, 'Admin'),
	(3, 'Gudang'),
	(4, 'Menu'),
	(5, 'Leader');
/*!40000 ALTER TABLE `user_menu` ENABLE KEYS */;

-- Dumping structure for table si_om_guwang.user_role
CREATE TABLE IF NOT EXISTS `user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table si_om_guwang.user_role: ~4 rows (approximately)
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` (`id`, `role`) VALUES
	(1, 'Root'),
	(2, 'Gudang'),
	(3, 'Leader'),
	(4, 'Admin');
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;

-- Dumping structure for table si_om_guwang.user_sub_menu
CREATE TABLE IF NOT EXISTS `user_sub_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `url` varchar(128) NOT NULL,
  `icon` varchar(128) NOT NULL,
  `is_active` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- Dumping data for table si_om_guwang.user_sub_menu: ~10 rows (approximately)
/*!40000 ALTER TABLE `user_sub_menu` DISABLE KEYS */;
INSERT INTO `user_sub_menu` (`id`, `menu_id`, `title`, `url`, `icon`, `is_active`) VALUES
	(1, 1, 'Dashboard', 'admin', 'fas fa-fw fa-tachometer-alt', 1),
	(4, 4, 'Menu Management', 'menu', 'fa-solid fa-fw fa-grip', 1),
	(5, 4, 'Sub Menu Management', 'menu/submenu', 'fa-solid fa-fw fa-bars', 1),
	(12, 1, 'User Management', 'usermanagement', 'fa-solid fa-fw fa-users', 1),
	(13, 1, 'Permintaan Material', 'masterpermintaan', 'fa-solid fa-toolbox fa-fw', 1),
	(14, 3, 'Master Barang', 'masterbarang', 'fa-solid fa-warehouse fa-fw', 1),
	(15, 3, 'Stock Barang Masuk', 'masterstock', 'fa-solid fa-cubes fa-fw', 1),
	(16, 5, 'Laporan Order Material', 'masterlaporan', 'fa-solid fa-file-signature fa-fw', 1),
	(17, 5, 'Stock Barang', 'masterbarang', 'fa-solid fa-cubes fa-fw', 1),
	(18, 3, 'Permintaan Material', 'masterpermintaan', 'fa-solid fa-toolbox fa-fw', 1);
/*!40000 ALTER TABLE `user_sub_menu` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
